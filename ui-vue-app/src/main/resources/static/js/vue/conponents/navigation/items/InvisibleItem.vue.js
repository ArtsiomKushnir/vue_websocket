export default {
    name: 'InvisibleItem',
    template:
        '<li class="nav-item d-none d-xl-block invisible flex-xl-grow-1">' +
        '   <a class="nav-link py-0 py-lg-8" href="#" title="">' +
        '       <div class="icon icon-xl">' +
        '           <svg xmlns="http://www.w3.org/2000/svg" ' +
        '               width="24" ' +
        '               height="24" ' +
        '               viewBox="0 0 24 24" ' +
        '               fill="none" ' +
        '               stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">' +
        '               <line x1="18" y1="6" x2="6" y2="18"></line>' +
        '               <line x1="6" y1="6" x2="18" y2="18"></line>' +
        '           </svg>' +
        '       </div>' +
        '   </a>' +
        '</li>'
}