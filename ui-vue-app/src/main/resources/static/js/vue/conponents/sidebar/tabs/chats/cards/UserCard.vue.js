export default {
    name: 'UserCard',
    props: {
        user: Object,
        wrapped: Object
    },
    methods: {
        selectUser(user) {
            this.$emit('select-user', user);
        },
        isSelected() {
            if (this.wrapped.selectedUser) {
                return this.user.name === this.wrapped.selectedUser.name;
            }
        }
    },
    template:
        '<a href="#!" class="card border-0 text-reset" ' +
        '   :class="{ selected: isSelected() }" ' +
        '   :key="user.name" ' +
        '   @click="selectUser(user)" >' +

        '   <div class="card-body">' +
        '       <div class="row gx-5">' +
        '           <div class="col-auto">' +
        '               <div class="avatar avatar-online">' +
        '                   <span class="avatar-text bg-success">{{ user.name[0].toUpperCase() }}</span>' +
        '               </div>' +
        '           </div>' +
        '           <div class="col">' +
        '               <div class="d-flex align-items-center mb-3">' +
        '                   <h5 class="me-auto mb-0">' +
        '                       {{ user.name }}' +
        '                   </h5>' +
        '                   <span class="text-muted extra-small ms-2">' +
        '                       {{ user.lastTimeInOnLine }}' +
        '                   </span>' +
        '               </div>' +
/*
        '               <!-- No messages -->' +
        '               <div class="d-flex align-items-center">' +
        '                   <div class="line-clamp me-auto">' +
        '                   </div>' +
        '               </div>' +

        '               <!-- Typing -->' +
        '               <div class="d-flex align-items-center">' +
        '                   <div class="line-clamp me-auto">' +
        '                       is typing<span class="typing-dots"><span>.</span><span>.</span><span>.</span></span>' +
        '                   </div>' +
        '                   <!-- Count unread messages -->' +
        '                   <div class="badge badge-circle bg-primary ms-5">' +
        '                       <span>3</span>' +
        '                   </div>' +
        '               </div>' +

        '               <!-- With last message -->' +
        '               <div class="d-flex align-items-center">' +
        '                   <div class="line-clamp me-auto">' +
        '                       Hello! Yeah, I\'m going to meet friend of mine at the departments stores now.' +
        '                   </div>' +
        '                   <!-- Count unread messages -->' +
        '                   <div class="badge badge-circle bg-primary ms-5">' +
        '                       <span>3</span>' +
        '                   </div>' +
        '               </div>' +
        */
        '           </div>' +
        '       </div>' +
        '   </div>' +
        '</a>'
}