import Brand from './brand/Brand.vue.js'

import InvisibleItem from './items/InvisibleItem.vue.js'
import CreateChatItem from './items/CreateChatItem.vue.js'
import ContactsItem from "./items/ContactsItem.vue.js"
import ChatsItem from './items/ChatsItem.vue.js'
import SettingsItem from './items/SettingsItem.vue.js'
import ProfileItem from './items/ProfileItem.vue.js'

export default {
    name: 'Navigation',
    components: {
        Brand,
        InvisibleItem,
        CreateChatItem,
        ContactsItem,
        ChatsItem,
        SettingsItem,
        ProfileItem
    },
    template:
        '<nav class="navigation d-flex flex-column text-center navbar navbar-light hide-scrollbar">' +
        '   <!-- Brand -->' +
        '   <brand></brand>' +

        '   <!-- Nav items -->' +
        '   <ul class="d-flex nav navbar-nav flex-row flex-xl-column flex-grow-1 justify-content-between justify-content-xl-center align-items-center w-100 py-4 py-lg-2 px-lg-3" role="tablist">' +
        '       <!-- Invisible item to center nav vertically -->' +
        '       <invisible-item></invisible-item>' +

        '       <!-- Create chat -->' +
        '       <!--<create-chat-item></create-chat-item>-->' +
        '       <!-- Friends -->' +
        '       <!--<contacts-item></contacts-item>-->' +
        '       <!-- Chats -->' +
        '       <chats-item></chats-item>' +

        '       <!-- Settings -->' +
        '       <settings-item></settings-item>' +
        '       <!-- Profile -->' +
        '       <profile-item></profile-item>' +

        '   </ul>' +
        '</nav>'
}