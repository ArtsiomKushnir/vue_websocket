export default {
    name: 'ProfileItem',
    template:
        '<li class="nav-item d-none d-xl-block">' +
        '   <a href="#" class="nav-link p-0 mt-lg-2" data-bs-toggle="modal" data-bs-target="#modal-profile">' +
        '       <div class="avatar avatar-online mx-auto">' +
        '           <span class="avatar-text bg-success">P</span>' +
        '       </div>' +
        '   </a>' +
        '</li>'
}