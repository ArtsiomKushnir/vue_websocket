import BlankChat from "./BlankChat.vue.js";
import Chat from "./Chat.vue.js";

export default {
    name: 'ChatMain',
    props: {
        messages: Array,
        wrapped: Object
    },
    methods: {
        sendMessage(message) {
            this.$emit('send-message', message);
        }
    },
    components: {
        BlankChat,
        Chat
    },
    template:
        '<main class="main is-visible" data-dropzone-area="">' +
        '   <blank-chat v-if="wrapped.selectedUser == null"></blank-chat>' +
        '   <chat v-else v-on:send-message="sendMessage" :messages="messages" :wrapped="wrapped" ></chat>' +
        '</main>'
}