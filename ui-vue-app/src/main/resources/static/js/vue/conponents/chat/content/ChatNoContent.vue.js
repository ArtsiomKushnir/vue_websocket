export default {
    name: 'ChatNoContent',
    template:
        '<div class="chat-body hide-scrollbar flex-1 h-100">' +
        '   <div class="chat-body-inner h-100" style="padding-bottom: 87px">' +
        '       <div class="d-flex flex-column h-100 justify-content-center">' +
        '           <div class="text-center mb-6">' +
        '               <span class="icon icon-xl text-muted">' +
        '                   <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" ' +
        '                       stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" ' +
        '                       class="feather feather-send">' +
        '                       <line x1="22" y1="2" x2="11" y2="13"></line>' +
        '                       <polygon points="22 2 15 22 11 13 2 9 22 2"></polygon>' +
        '                   </svg>' +
        '               </span>' +
        '           </div>' +
        '           <p class="text-center text-muted">No messages yet, <br> start the conversation!</p>' +
        '       </div>' +
        '   </div>' +
        '</div>'
}