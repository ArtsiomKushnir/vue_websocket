export default {
    name: 'Divider',
    props: ['date'],
    template:
        '<div class="message-divider">' +
        '   <small class="text-muted">{{ date }}</small>' +
        '</div>'
}