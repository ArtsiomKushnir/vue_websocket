export default {
    name: 'CreateTab',
    template:
        '<div class="tab-pane fade h-100" id="tab-content-create-chat" role="tabpanel">' +
        '<div class="d-flex flex-column h-100">\n' +
        '          <div class="hide-scrollbar">\n' +

        '            <div class="container py-8">\n' +

        '              <!-- Title -->\n' +
        '              <div class="mb-8">\n' +
        '                <h2 class="fw-bold m-0">Create chat</h2>\n' +
        '              </div>\n' +

        '              <!-- Search -->\n' +
        '              <div class="mb-6">\n' +
        '                <div class="mb-5">\n' +
        '                  <form action="#">\n' +
        '                    <div class="input-group">\n' +
        '                      <div class="input-group-text">\n' +
        '                        <div class="icon icon-lg">\n' +
        '                          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>\n' +
        '                        </div>\n' +
        '                      </div>\n' +

        '                      <input type="text" class="form-control form-control-lg ps-0" placeholder="Search messages or users" aria-label="Search for messages or users...">\n' +
        '                    </div>\n' +
        '                  </form>\n' +
        '                </div>\n' +
        '\n' +
        '                <ul class="nav nav-pills nav-justified" role="tablist">\n' +
        '                  <li class="nav-item">\n' +
        '                    <a class="nav-link active" data-bs-toggle="pill" href="#create-chat-info" role="tab" aria-controls="create-chat-info" aria-selected="true">\n' +
        '                      Details\n' +
        '                    </a>\n' +
        '                  </li>\n' +
        '\n' +
        '                  <li class="nav-item">\n' +
        '                    <a class="nav-link" data-bs-toggle="pill" href="#create-chat-members" role="tab" aria-controls="create-chat-members" aria-selected="true">\n' +
        '                      People\n' +
        '                    </a>\n' +
        '                  </li>\n' +
        '                </ul>\n' +
        '              </div>\n' +
        '\n' +
        '              <!-- Tabs content -->\n' +
        '              <div class="tab-content" role="tablist">\n' +
        '                <div class="tab-pane fade show active" id="create-chat-info" role="tabpanel">\n' +
        '\n' +
        '                  <div class="card border-0">\n' +
        '                    <div class="profile">\n' +
        '                      <div class="profile-img text-primary rounded-top">\n' +
        '                        <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 400 140.74"><defs><style>.cls-2{fill:#fff;opacity:0.1;}</style></defs><g><g><path d="M400,125A1278.49,1278.49,0,0,1,0,125V0H400Z"></path><path class="cls-2" d="M361.13,128c.07.83.15,1.65.27,2.46h0Q380.73,128,400,125V87l-1,0a38,38,0,0,0-38,38c0,.86,0,1.71.09,2.55C361.11,127.72,361.12,127.88,361.13,128Z"></path><path class="cls-2" d="M12.14,119.53c.07.79.15,1.57.26,2.34v0c.13.84.28,1.66.46,2.48l.07.3c.18.8.39,1.59.62,2.37h0q33.09,4.88,66.36,8,.58-1,1.09-2l.09-.18a36.35,36.35,0,0,0,1.81-4.24l.08-.24q.33-.94.6-1.9l.12-.41a36.26,36.26,0,0,0,.91-4.42c0-.19,0-.37.07-.56q.11-.86.18-1.73c0-.21,0-.42,0-.63,0-.75.08-1.51.08-2.28a36.5,36.5,0,0,0-73,0c0,.83,0,1.64.09,2.45C12.1,119.15,12.12,119.34,12.14,119.53Z"></path><circle class="cls-2" cx="94.5" cy="57.5" r="22.5"></circle><path class="cls-2" d="M276,0a43,43,0,0,0,43,43A43,43,0,0,0,362,0Z"></path></g></g></svg>\n' +
        '                      </div>\n' +
        '\n' +
        '                      <div class="profile-body p-0">\n' +
        '                        <div class="avatar avatar-lg">\n' +
        '                                                            <span class="avatar-text bg-primary">\n' +
        '                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-image"><rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect><circle cx="8.5" cy="8.5" r="1.5"></circle><polyline points="21 15 16 10 5 21"></polyline></svg>\n' +
        '                                                            </span>\n' +
        '\n' +
        '                          <div class="badge badge-lg badge-circle bg-primary border-outline position-absolute bottom-0 end-0">\n' +
        '                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus"><line x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19" y2="12"></line></svg>\n' +
        '                          </div>\n' +
        '\n' +
        '                          <input id="upload-chat-img" class="d-none" type="file">\n' +
        '                          <label class="stretched-label mb-0" for="upload-chat-img"></label>\n' +
        '                        </div>\n' +
        '                      </div>\n' +
        '                    </div>\n' +
        '\n' +
        '                    <div class="card-body">\n' +
        '                      <form autocomplete="off">\n' +
        '                        <div class="row gy-6">\n' +
        '                          <div class="col-12">\n' +
        '                            <div class="form-floating">\n' +
        '                              <input type="text" class="form-control" id="floatingInput" placeholder="Enter a chat name">\n' +
        '                              <label for="floatingInput">Enter group name</label>\n' +
        '                            </div>\n' +
        '                          </div>\n' +
        '                          <div class="col-12">\n' +
        '                            <div class="form-floating">\n' +
        '                              <textarea class="form-control" placeholder="Description" id="floatingTextarea" rows="8" data-autosize="true" style="min-height: 100px; overflow: hidden; overflow-wrap: break-word; resize: none;"></textarea>\n' +
        '                              <label for="floatingTextarea">What\'s your purpose?</label>\n' +
        '                            </div>\n' +
        '                          </div>\n' +
        '                        </div>\n' +
        '                      </form>\n' +
        '                    </div>\n' +
        '                  </div>\n' +
        '\n' +
        '                  <div class="d-flex align-items-center mt-4 px-6">\n' +
        '                    <small class="text-muted me-auto">Enter chat name and add an optional photo.</small>\n' +
        '                  </div>\n' +
        '\n' +
        '                  <!-- Options -->\n' +
        '                  <div class="mt-8">\n' +
        '                    <div class="d-flex align-items-center mb-4 px-6">\n' +
        '                      <small class="text-muted me-auto">Options</small>\n' +
        '                    </div>\n' +
        '\n' +
        '                    <div class="card border-0">\n' +
        '                      <div class="card-body">\n' +
        '                        <div class="row gx-5">\n' +
        '                          <div class="col-auto">\n' +
        '                            <div class="btn btn-sm btn-icon btn-dark">\n' +
        '                              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-lock"><rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect><path d="M7 11V7a5 5 0 0 1 10 0v4"></path></svg>\n' +
        '                            </div>\n' +
        '                          </div>\n' +
        '                          <div class="col">\n' +
        '                            <h5>Make Private</h5>\n' +
        '                            <p>Can only be viewed by invites</p>\n' +
        '                          </div>\n' +
        '                          <div class="col-auto align-self-center">\n' +
        '                            <div class="form-check form-switch ps-0">\n' +
        '                              <input class="form-check-input" type="checkbox" id="new-chat-options-1">\n' +
        '                              <label class="form-check-label" for="new-chat-options-1"></label>\n' +
        '                            </div>\n' +
        '                          </div>\n' +
        '                        </div>\n' +
        '                      </div>\n' +
        '                    </div>\n' +
        '                  </div>\n' +
        '                </div>\n' +
        '\n' +
        '                <!-- Members -->\n' +
        '                <div class="tab-pane fade" id="create-chat-members" role="tabpanel">\n' +
        '                  <nav>\n' +
        '                    <div class="my-5">\n' +
        '                      <small class="text-uppercase text-muted">B</small>\n' +
        '                    </div>\n' +
        '\n' +
        '                    <!-- Card -->\n' +
        '                    <div class="card border-0 mt-5">\n' +
        '                      <div class="card-body">\n' +
        '\n' +
        '                        <div class="row align-items-center gx-5">\n' +
        '                          <div class="col-auto">\n' +
        '                            <div class="avatar ">\n' +
        '\n' +
        '                              <img class="avatar-img" src="/img/avatars/6.jpg" alt="">\n' +
        '\n' +
        '\n' +
        '                            </div>\n' +
        '                          </div>\n' +
        '                          <div class="col">\n' +
        '                            <h5>Bill Marrow</h5>\n' +
        '                            <p>last seen 3 days ago</p>\n' +
        '                          </div>\n' +
        '                          <div class="col-auto">\n' +
        '                            <div class="form-check">\n' +
        '                              <input class="form-check-input" type="checkbox" value="" id="id-member-1">\n' +
        '                              <label class="form-check-label" for="id-member-1"></label>\n' +
        '                            </div>\n' +
        '                          </div>\n' +
        '                        </div>\n' +
        '                        <label class="stretched-label" for="id-member-1"></label>\n' +
        '                      </div>\n' +
        '                    </div>\n' +
        '                    <!-- Card -->\n' +
        '\n' +
        '                    <div class="my-5">\n' +
        '                      <small class="text-uppercase text-muted">D</small>\n' +
        '                    </div>\n' +
        '\n' +
        '                    <!-- Card -->\n' +
        '                    <div class="card border-0 mt-5">\n' +
        '                      <div class="card-body">\n' +
        '\n' +
        '                        <div class="row align-items-center gx-5">\n' +
        '                          <div class="col-auto">\n' +
        '                            <div class="avatar ">\n' +
        '\n' +
        '                              <img class="avatar-img" src="/img/avatars/5.jpg" alt="">\n' +
        '\n' +
        '\n' +
        '                            </div>\n' +
        '                          </div>\n' +
        '                          <div class="col">\n' +
        '                            <h5>Damian Binder</h5>\n' +
        '                            <p>last seen within a week</p>\n' +
        '                          </div>\n' +
        '                          <div class="col-auto">\n' +
        '                            <div class="form-check">\n' +
        '                              <input class="form-check-input" type="checkbox" value="" id="id-member-2">\n' +
        '                              <label class="form-check-label" for="id-member-2"></label>\n' +
        '                            </div>\n' +
        '                          </div>\n' +
        '                        </div>\n' +
        '                        <label class="stretched-label" for="id-member-2"></label>\n' +
        '                      </div>\n' +
        '                    </div>\n' +
        '                    <!-- Card --><!-- Card -->\n' +
        '                    <div class="card border-0 mt-5">\n' +
        '                      <div class="card-body">\n' +
        '\n' +
        '                        <div class="row align-items-center gx-5">\n' +
        '                          <div class="col-auto">\n' +
        '                            <div class="avatar avatar-online">\n' +
        '\n' +
        '\n' +
        '                              <span class="avatar-text">D</span>\n' +
        '\n' +
        '                            </div>\n' +
        '                          </div>\n' +
        '                          <div class="col">\n' +
        '                            <h5>Don Knight</h5>\n' +
        '                            <p>online</p>\n' +
        '                          </div>\n' +
        '                          <div class="col-auto">\n' +
        '                            <div class="form-check">\n' +
        '                              <input class="form-check-input" type="checkbox" value="" id="id-member-3">\n' +
        '                              <label class="form-check-label" for="id-member-3"></label>\n' +
        '                            </div>\n' +
        '                          </div>\n' +
        '                        </div>\n' +
        '                        <label class="stretched-label" for="id-member-3"></label>\n' +
        '                      </div>\n' +
        '                    </div>\n' +
        '                    <!-- Card -->\n' +
        '\n' +
        '                    <div class="my-5">\n' +
        '                      <small class="text-uppercase text-muted">E</small>\n' +
        '                    </div>\n' +
        '\n' +
        '                    <!-- Card -->\n' +
        '                    <div class="card border-0 mt-5">\n' +
        '                      <div class="card-body">\n' +
        '\n' +
        '                        <div class="row align-items-center gx-5">\n' +
        '                          <div class="col-auto">\n' +
        '                            <div class="avatar avatar-online">\n' +
        '\n' +
        '                              <img class="avatar-img" src="/img/avatars/8.jpg" alt="">\n' +
        '\n' +
        '\n' +
        '                            </div>\n' +
        '                          </div>\n' +
        '                          <div class="col">\n' +
        '                            <h5>Elise Dennis</h5>\n' +
        '                            <p>online</p>\n' +
        '                          </div>\n' +
        '                          <div class="col-auto">\n' +
        '                            <div class="form-check">\n' +
        '                              <input class="form-check-input" type="checkbox" value="" id="id-member-4">\n' +
        '                              <label class="form-check-label" for="id-member-4"></label>\n' +
        '                            </div>\n' +
        '                          </div>\n' +
        '                        </div>\n' +
        '                        <label class="stretched-label" for="id-member-4"></label>\n' +
        '                      </div>\n' +
        '                    </div>\n' +
        '                    <!-- Card -->\n' +
        '\n' +
        '                    <div class="my-5">\n' +
        '                      <small class="text-uppercase text-muted">M</small>\n' +
        '                    </div>\n' +
        '\n' +
        '                    <!-- Card -->\n' +
        '                    <div class="card border-0 mt-5">\n' +
        '                      <div class="card-body">\n' +
        '\n' +
        '                        <div class="row align-items-center gx-5">\n' +
        '                          <div class="col-auto">\n' +
        '                            <div class="avatar ">\n' +
        '\n' +
        '\n' +
        '                              <span class="avatar-text">M</span>\n' +
        '\n' +
        '                            </div>\n' +
        '                          </div>\n' +
        '                          <div class="col">\n' +
        '                            <h5>Marshall Wallaker</h5>\n' +
        '                            <p>last seen within a month</p>\n' +
        '                          </div>\n' +
        '                          <div class="col-auto">\n' +
        '                            <div class="form-check">\n' +
        '                              <input class="form-check-input" type="checkbox" value="" id="id-member-6">\n' +
        '                              <label class="form-check-label" for="id-member-6"></label>\n' +
        '                            </div>\n' +
        '                          </div>\n' +
        '                        </div>\n' +
        '                        <label class="stretched-label" for="id-member-6"></label>\n' +
        '                      </div>\n' +
        '                    </div>\n' +
        '                    <!-- Card --><!-- Card -->\n' +
        '                    <div class="card border-0 mt-5">\n' +
        '                      <div class="card-body">\n' +
        '\n' +
        '                        <div class="row align-items-center gx-5">\n' +
        '                          <div class="col-auto">\n' +
        '                            <div class="avatar ">\n' +
        '\n' +
        '                              <img class="avatar-img" src="/img/avatars/11.jpg" alt="">\n' +
        '\n' +
        '\n' +
        '                            </div>\n' +
        '                          </div>\n' +
        '                          <div class="col">\n' +
        '                            <h5>Mila White</h5>\n' +
        '                            <p>last seen a long time ago</p>\n' +
        '                          </div>\n' +
        '                          <div class="col-auto">\n' +
        '                            <div class="form-check">\n' +
        '                              <input class="form-check-input" type="checkbox" value="" id="id-member-5">\n' +
        '                              <label class="form-check-label" for="id-member-5"></label>\n' +
        '                            </div>\n' +
        '                          </div>\n' +
        '                        </div>\n' +
        '                        <label class="stretched-label" for="id-member-5"></label>\n' +
        '                      </div>\n' +
        '                    </div>\n' +
        '                    <!-- Card -->\n' +
        '\n' +
        '                    <div class="my-5">\n' +
        '                      <small class="text-uppercase text-muted">O</small>\n' +
        '                    </div>\n' +
        '\n' +
        '                    <!-- Card -->\n' +
        '                    <div class="card border-0 mt-5">\n' +
        '                      <div class="card-body">\n' +
        '\n' +
        '                        <div class="row align-items-center gx-5">\n' +
        '                          <div class="col-auto">\n' +
        '                            <div class="avatar avatar-online">\n' +
        '\n' +
        '\n' +
        '                              <span class="avatar-text">O</span>\n' +
        '\n' +
        '                            </div>\n' +
        '                          </div>\n' +
        '                          <div class="col">\n' +
        '                            <h5>Ollie Chandler</h5>\n' +
        '                            <p>online</p>\n' +
        '                          </div>\n' +
        '                          <div class="col-auto">\n' +
        '                            <div class="form-check">\n' +
        '                              <input class="form-check-input" type="checkbox" value="" id="id-member-7">\n' +
        '                              <label class="form-check-label" for="id-member-7"></label>\n' +
        '                            </div>\n' +
        '                          </div>\n' +
        '                        </div>\n' +
        '                        <label class="stretched-label" for="id-member-7"></label>\n' +
        '                      </div>\n' +
        '                    </div>\n' +
        '                    <!-- Card -->\n' +
        '\n' +
        '                    <div class="my-5">\n' +
        '                      <small class="text-uppercase text-muted">W</small>\n' +
        '                    </div>\n' +
        '\n' +
        '                    <!-- Card -->\n' +
        '                    <div class="card border-0 mt-5">\n' +
        '                      <div class="card-body">\n' +
        '\n' +
        '                        <div class="row align-items-center gx-5">\n' +
        '                          <div class="col-auto">\n' +
        '                            <div class="avatar ">\n' +
        '\n' +
        '                              <img class="avatar-img" src="/img/avatars/4.jpg" alt="">\n' +
        '\n' +
        '\n' +
        '                            </div>\n' +
        '                          </div>\n' +
        '                          <div class="col">\n' +
        '                            <h5>Warren White</h5>\n' +
        '                            <p>last seen recently</p>\n' +
        '                          </div>\n' +
        '                          <div class="col-auto">\n' +
        '                            <div class="form-check">\n' +
        '                              <input class="form-check-input" type="checkbox" value="" id="id-member-8">\n' +
        '                              <label class="form-check-label" for="id-member-8"></label>\n' +
        '                            </div>\n' +
        '                          </div>\n' +
        '                        </div>\n' +
        '                        <label class="stretched-label" for="id-member-8"></label>\n' +
        '                      </div>\n' +
        '                    </div>\n' +
        '                    <!-- Card --><!-- Card -->\n' +
        '                    <div class="card border-0 mt-5">\n' +
        '                      <div class="card-body">\n' +
        '\n' +
        '                        <div class="row align-items-center gx-5">\n' +
        '                          <div class="col-auto">\n' +
        '                            <div class="avatar avatar-online">\n' +
        '\n' +
        '                              <img class="avatar-img" src="/img/avatars/7.jpg" alt="">\n' +
        '\n' +
        '\n' +
        '                            </div>\n' +
        '                          </div>\n' +
        '                          <div class="col">\n' +
        '                            <h5>William Wright</h5>\n' +
        '                            <p>online</p>\n' +
        '                          </div>\n' +
        '                          <div class="col-auto">\n' +
        '                            <div class="form-check">\n' +
        '                              <input class="form-check-input" type="checkbox" value="" id="id-member-9">\n' +
        '                              <label class="form-check-label" for="id-member-9"></label>\n' +
        '                            </div>\n' +
        '                          </div>\n' +
        '                        </div>\n' +
        '                        <label class="stretched-label" for="id-member-9"></label>\n' +
        '                      </div>\n' +
        '                    </div>\n' +
        '                    <!-- Card -->\n' +
        '                  </nav>\n' +
        '                </div>\n' +
        '              </div>\n' +
        '              <!-- Tabs content -->\n' +
        '            </div>\n' +

        '          </div>\n' +

        '          <!-- Button -->\n' +
        '          <div class="container mt-n4 mb-8 position-relative">\n' +
        '            <button class="btn btn-lg btn-primary w-100 d-flex align-items-center" type="button">\n' +
        '              Start chat\n' +
        '              <span class="icon ms-auto">\n' +
        '                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>\n' +
        '                                    </span>\n' +
        '            </button>\n' +
        '          </div>\n' +
        '          <!-- Button -->\n' +
        '        </div>' +
        '</div>'
}