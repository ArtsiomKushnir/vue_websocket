import ChatHeader from './ChatHeader.vue.js'
import ChatContent from './content/ChatContent.vue.js'
import ChatNoContent from './content/ChatNoContent.vue.js'
import FormTextArea from './FormTextArea.vue.js'

export default {
    name: 'Chat',
    props: {
        messages: Array,
        wrapped: Object
    },
    methods: {
        sendMessage(message) {
            this.$emit('send-message', message);
        }
    },
    components: {
        ChatHeader,
        ChatContent,
        ChatNoContent,
        FormTextArea
    },
    template:
        '<div class="container h-100">' +
        '   <div class="d-flex flex-column h-100 position-relative">' +

        '       <!-- Chat: Header -->' +
        '       <chat-header :selectedUser="wrapped.selectedUser"></chat-header>' +

        '       <!-- Content -->' +
        '       <chat-content ' +
        '           :messages="messages" :selectedUser="wrapped.selectedUser" >' +
        '       </chat-content>' +
        '       <!--<chat-no-content v-else></chat-no-content>-->' +

        '       <!-- Chat: Footer -->' +
        '       <div class="chat-footer pb-3 pb-lg-7 position-absolute bottom-0 start-0">' +
        '           <!-- Chat: Files -->' +
        '           <div class="dz-preview bg-dark" id="dz-preview-row" data-horizontal-scroll="">' +
        '           </div>' +

        '           <!-- Chat: Form -->' +
        '           <form-text-area v-on:send-message="sendMessage" :selectedUser="wrapped.selectedUser" ></form-text-area>' +

        '       </div>' +
        '   </div>' +
        '</div>'
}