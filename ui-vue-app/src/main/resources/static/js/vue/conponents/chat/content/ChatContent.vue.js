import Message from '../message/Message.vue.js'
import MessageOut from '../message/MessageOut.vue.js'
import Divider from '../message/Divider.vue.js'

export default {
    name: 'ChatContent',
    props: {
        messages: Array,
        selectedUser: Object
    },
    components: {
        Message,
        MessageOut,
        Divider
    },
    template:
        '<div class="chat-body hide-scrollbar flex-1 h-100">' +
        '   <div class="chat-body-inner" style="padding-bottom: 88px">' +
        '       <div class="py-6 py-lg-12">' +

        '           <template v-for="message in messages" >' +

        '               <message-out ' +
        '                   v-if=" selectedUser.name !== message.sender "' +
        '                   :message="message" >' +
        '               </message-out>' +

        '               <message ' +
        '                   v-else' +
        '                   :message="message" >' +
        '               </message>' +

        '           </template>' +

        '       </div>' +
        '   </div>' +
        '</div>'
}