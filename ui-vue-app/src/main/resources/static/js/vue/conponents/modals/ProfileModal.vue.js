export default {
    name: 'ProfileModal',
    template:
        '<div class="modal fade" id="modal-profile" tabindex="-1" aria-labelledby="modal-profile" aria-hidden="true">\n' +
        '  <div class="modal-dialog modal-dialog-centered modal-fullscreen-xl-down">\n' +
        '    <div class="modal-content">\n' +
        '\n' +
        '      <!-- Modal body -->\n' +
        '      <div class="modal-body py-0">\n' +
        '        <!-- Header -->\n' +
        '        <div class="profile modal-gx-n">\n' +
        '          <div class="profile-img text-primary rounded-top-xl">\n' +
        '            <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 400 140.74"><defs><style>.cls-2{fill:#fff;opacity:0.1;}</style></defs><g><g><path d="M400,125A1278.49,1278.49,0,0,1,0,125V0H400Z"></path><path class="cls-2" d="M361.13,128c.07.83.15,1.65.27,2.46h0Q380.73,128,400,125V87l-1,0a38,38,0,0,0-38,38c0,.86,0,1.71.09,2.55C361.11,127.72,361.12,127.88,361.13,128Z"></path><path class="cls-2" d="M12.14,119.53c.07.79.15,1.57.26,2.34v0c.13.84.28,1.66.46,2.48l.07.3c.18.8.39,1.59.62,2.37h0q33.09,4.88,66.36,8,.58-1,1.09-2l.09-.18a36.35,36.35,0,0,0,1.81-4.24l.08-.24q.33-.94.6-1.9l.12-.41a36.26,36.26,0,0,0,.91-4.42c0-.19,0-.37.07-.56q.11-.86.18-1.73c0-.21,0-.42,0-.63,0-.75.08-1.51.08-2.28a36.5,36.5,0,0,0-73,0c0,.83,0,1.64.09,2.45C12.1,119.15,12.12,119.34,12.14,119.53Z"></path><circle class="cls-2" cx="94.5" cy="57.5" r="22.5"></circle><path class="cls-2" d="M276,0a43,43,0,0,0,43,43A43,43,0,0,0,362,0Z"></path></g></g></svg>\n' +
        '\n' +
        '            <div class="position-absolute top-0 start-0 py-6 px-5">\n' +
        '              <button type="button" class="btn-close btn-close-white btn-close-arrow opacity-100" data-bs-dismiss="modal" aria-label="Close"></button>\n' +
        '            </div>\n' +
        '          </div>\n' +
        '\n' +
        '          <div class="profile-body">\n' +
        '            <div class="avatar avatar-xl">\n' +
        '              <img class="avatar-img" src="/img/avatars/1.jpg" alt="#">\n' +
        '            </div>\n' +
        '\n' +
        '            <h4 class="mb-1">William Wright</h4>\n' +
        '            <p>last seen 5 minutes ago</p>\n' +
        '          </div>\n' +
        '        </div>\n' +
        '        <!-- Header -->\n' +
        '\n' +
        '        <hr class="hr-bold modal-gx-n my-0">\n' +
        '\n' +
        '        <!-- List -->\n' +
        '        <ul class="list-group list-group-flush">\n' +
        '          <li class="list-group-item">\n' +
        '            <div class="row align-items-center gx-6">\n' +
        '              <div class="col">\n' +
        '                <h5>Location</h5>\n' +
        '                <p>USA, Houston</p>\n' +
        '              </div>\n' +
        '\n' +
        '              <div class="col-auto">\n' +
        '                <div class="btn btn-sm btn-icon btn-dark">\n' +
        '                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-globe"><circle cx="12" cy="12" r="10"></circle><line x1="2" y1="12" x2="22" y2="12"></line><path d="M12 2a15.3 15.3 0 0 1 4 10 15.3 15.3 0 0 1-4 10 15.3 15.3 0 0 1-4-10 15.3 15.3 0 0 1 4-10z"></path></svg>\n' +
        '                </div>\n' +
        '              </div>\n' +
        '            </div>\n' +
        '          </li>\n' +
        '\n' +
        '          <li class="list-group-item">\n' +
        '            <div class="row align-items-center gx-6">\n' +
        '              <div class="col">\n' +
        '                <h5>E-mail</h5>\n' +
        '                <p>william@studio.com</p>\n' +
        '              </div>\n' +
        '\n' +
        '              <div class="col-auto">\n' +
        '                <div class="btn btn-sm btn-icon btn-dark">\n' +
        '                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-mail"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg>\n' +
        '                </div>\n' +
        '              </div>\n' +
        '            </div>\n' +
        '          </li>\n' +
        '\n' +
        '          <li class="list-group-item">\n' +
        '            <div class="row align-items-center gx-6">\n' +
        '              <div class="col">\n' +
        '                <h5>Phone</h5>\n' +
        '                <p>1-800-275-2273</p>\n' +
        '              </div>\n' +
        '\n' +
        '              <div class="col-auto">\n' +
        '                <div class="btn btn-sm btn-icon btn-dark">\n' +
        '                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-phone-call"><path d="M15.05 5A5 5 0 0 1 19 8.95M15.05 1A9 9 0 0 1 23 8.94m-1 7.98v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path></svg>\n' +
        '                </div>\n' +
        '              </div>\n' +
        '            </div>\n' +
        '          </li>\n' +
        '        </ul>\n' +
        '        <!-- List  -->\n' +
        '\n' +
        '        <hr class="hr-bold modal-gx-n my-0">\n' +
        '\n' +
        '        <!-- List -->\n' +
        '        <ul class="list-group list-group-flush">\n' +
        '          <li class="list-group-item">\n' +
        '            <div class="row align-items-center gx-6">\n' +
        '              <div class="col">\n' +
        '                <h5>Active status</h5>\n' +
        '                <p>Show when you\'re active.</p>\n' +
        '              </div>\n' +
        '\n' +
        '              <div class="col-auto">\n' +
        '                <div class="form-check form-switch">\n' +
        '                  <input class="form-check-input" type="checkbox" id="profile-status" checked="">\n' +
        '                  <label class="form-check-label" for="profile-status"></label>\n' +
        '                </div>\n' +
        '              </div>\n' +
        '            </div>\n' +
        '          </li>\n' +
        '        </ul>\n' +
        '        <!-- List -->' +

        '        <hr class="hr-bold modal-gx-n my-0">' +

        '        <!-- List -->' +
        '        <ul class="list-group list-group-flush">' +
        '          <li class="list-group-item">' +
        '            <a href="/logout" class="text-danger">Logout</a>' +
        '          </li>' +
        '        </ul>' +
        '        <!-- List -->' +
        '      </div>\n' +
        '      <!-- Modal body -->' +

        '    </div>\n' +
        '  </div>\n' +
        '</div>'
}