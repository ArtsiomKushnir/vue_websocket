export default {
    name: 'ContactsItem',
    template:
        '<li class="nav-item">' +
        '   <a class="nav-link py-0 py-lg-8" id="tab-friends" href="#tab-content-friends" title="Friends" data-bs-toggle="tab" role="tab" aria-selected="false">' +
        '       <div class="icon icon-xl">' +
        '           <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" ' +
        '               stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" ' +
        '               class="feather feather-users">' +
        '               <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>' +
        '               <circle cx="9" cy="7" r="4"></circle>' +
        '               <path d="M23 21v-2a4 4 0 0 0-3-3.87"></path>' +
        '               <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>' +
        '           </svg>' +
        '       </div>' +
        '   </a>' +
        '</li>'
}