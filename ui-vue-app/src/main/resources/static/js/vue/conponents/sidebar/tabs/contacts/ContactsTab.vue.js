export default {
    name: 'ContactsTab',
    template:
        '<div class="tab-pane fade h-100" id="tab-content-friends" role="tabpanel">\n' +
        '        <div class="d-flex flex-column h-100">\n' +
        '          <div class="hide-scrollbar">\n' +
        '            <div class="container py-8">\n' +

        '              <!-- Title -->\n' +
        '              <div class="mb-8">\n' +
        '                <h2 class="fw-bold m-0">Friends</h2>\n' +
        '              </div>\n' +

        '              <!-- Search -->\n' +
        '              <div class="mb-6">\n' +
        '                <form action="#">\n' +
        '                  <div class="input-group">\n' +
        '                    <div class="input-group-text">\n' +
        '                      <div class="icon icon-lg">\n' +
        '                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>\n' +
        '                      </div>\n' +
        '                    </div>\n' +

        '                    <input type="text" class="form-control form-control-lg ps-0" placeholder="Search messages or users" aria-label="Search for messages or users...">\n' +
        '                  </div>\n' +
        '                </form>\n' +
        '\n' +
        '                <!-- Invite button -->\n' +
        '                <div class="mt-5">\n' +
        '                  <a href="#" class="btn btn-lg btn-primary w-100 d-flex align-items-center" data-bs-toggle="modal" data-bs-target="#modal-invite">\n' +
        '                    Find Friends\n' +
        '\n' +
        '                    <span class="icon ms-auto">\n' +
        '                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user-plus"><path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="8.5" cy="7" r="4"></circle><line x1="20" y1="8" x2="20" y2="14"></line><line x1="23" y1="11" x2="17" y2="11"></line></svg>\n' +
        '                                                </span>\n' +
        '                  </a>\n' +
        '                </div>\n' +
        '              </div>\n' +
        '\n' +
        '              <!-- List -->\n' +
        '              <div class="card-list">\n' +
        '                <div class="my-5">\n' +
        '                  <small class="text-uppercase text-muted">B</small>\n' +
        '                </div>\n' +
        '\n' +
        '                <!-- Card -->\n' +
        '                <div class="card border-0">\n' +
        '                  <div class="card-body">\n' +
        '\n' +
        '                    <div class="row align-items-center gx-5">\n' +
        '                      <div class="col-auto">\n' +
        '                        <a href="#" class="avatar ">\n' +
        '\n' +
        '                          <img class="avatar-img" src="/img/avatars/6.jpg" alt="">\n' +
        '\n' +
        '\n' +
        '                        </a>\n' +
        '                      </div>\n' +
        '\n' +
        '                      <div class="col">\n' +
        '                        <h5><a href="#">Bill Marrow</a></h5>\n' +
        '                        <p>last seen 3 days ago</p>\n' +
        '                      </div>\n' +
        '\n' +
        '                      <div class="col-auto">\n' +
        '                        <!-- Dropdown -->\n' +
        '                        <div class="dropdown">\n' +
        '                          <a class="icon text-muted" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">\n' +
        '                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg>\n' +
        '                          </a>\n' +
        '\n' +
        '                          <ul class="dropdown-menu">\n' +
        '                            <li><a class="dropdown-item" href="#">New message</a></li>\n' +
        '                            <li><a class="dropdown-item" href="#">Edit contact</a>\n' +
        '                            </li>\n' +
        '                            <li>\n' +
        '                              <hr class="dropdown-divider">\n' +
        '                            </li>\n' +
        '                            <li>\n' +
        '                              <a class="dropdown-item text-danger" href="#">Block user</a>\n' +
        '                            </li>\n' +
        '                          </ul>\n' +
        '                        </div>\n' +
        '                      </div>\n' +
        '\n' +
        '                    </div>\n' +
        '\n' +
        '                  </div>\n' +
        '                </div>\n' +
        '                <!-- Card -->\n' +
        '\n' +
        '                <div class="my-5">\n' +
        '                  <small class="text-uppercase text-muted">D</small>\n' +
        '                </div>\n' +
        '\n' +
        '                <!-- Card -->\n' +
        '                <div class="card border-0">\n' +
        '                  <div class="card-body">\n' +
        '\n' +
        '                    <div class="row align-items-center gx-5">\n' +
        '                      <div class="col-auto">\n' +
        '                        <a href="#" class="avatar ">\n' +
        '\n' +
        '                          <img class="avatar-img" src="/img/avatars/5.jpg" alt="">\n' +
        '\n' +
        '\n' +
        '                        </a>\n' +
        '                      </div>\n' +
        '\n' +
        '                      <div class="col">\n' +
        '                        <h5><a href="#">Damian Binder</a></h5>\n' +
        '                        <p>last seen within a week</p>\n' +
        '                      </div>\n' +
        '\n' +
        '                      <div class="col-auto">\n' +
        '                        <!-- Dropdown -->\n' +
        '                        <div class="dropdown">\n' +
        '                          <a class="icon text-muted" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">\n' +
        '                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg>\n' +
        '                          </a>\n' +
        '\n' +
        '                          <ul class="dropdown-menu">\n' +
        '                            <li><a class="dropdown-item" href="#">New message</a></li>\n' +
        '                            <li><a class="dropdown-item" href="#">Edit contact</a>\n' +
        '                            </li>\n' +
        '                            <li>\n' +
        '                              <hr class="dropdown-divider">\n' +
        '                            </li>\n' +
        '                            <li>\n' +
        '                              <a class="dropdown-item text-danger" href="#">Block user</a>\n' +
        '                            </li>\n' +
        '                          </ul>\n' +
        '                        </div>\n' +
        '                      </div>\n' +
        '\n' +
        '                    </div>\n' +
        '\n' +
        '                  </div>\n' +
        '                </div>\n' +
        '                <!-- Card --><!-- Card -->\n' +
        '                <div class="card border-0">\n' +
        '                  <div class="card-body">\n' +
        '\n' +
        '                    <div class="row align-items-center gx-5">\n' +
        '                      <div class="col-auto">\n' +
        '                        <a href="#" class="avatar avatar-online">\n' +
        '\n' +
        '\n' +
        '                          <span class="avatar-text">D</span>\n' +
        '\n' +
        '                        </a>\n' +
        '                      </div>\n' +
        '\n' +
        '                      <div class="col">\n' +
        '                        <h5><a href="#">Don Knight</a></h5>\n' +
        '                        <p>online</p>\n' +
        '                      </div>\n' +
        '\n' +
        '                      <div class="col-auto">\n' +
        '                        <!-- Dropdown -->\n' +
        '                        <div class="dropdown">\n' +
        '                          <a class="icon text-muted" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">\n' +
        '                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg>\n' +
        '                          </a>\n' +
        '\n' +
        '                          <ul class="dropdown-menu">\n' +
        '                            <li><a class="dropdown-item" href="#">New message</a></li>\n' +
        '                            <li><a class="dropdown-item" href="#">Edit contact</a>\n' +
        '                            </li>\n' +
        '                            <li>\n' +
        '                              <hr class="dropdown-divider">\n' +
        '                            </li>\n' +
        '                            <li>\n' +
        '                              <a class="dropdown-item text-danger" href="#">Block user</a>\n' +
        '                            </li>\n' +
        '                          </ul>\n' +
        '                        </div>\n' +
        '                      </div>\n' +
        '\n' +
        '                    </div>\n' +
        '\n' +
        '                  </div>\n' +
        '                </div>\n' +
        '                <!-- Card -->\n' +
        '\n' +
        '                <div class="my-5">\n' +
        '                  <small class="text-uppercase text-muted">E</small>\n' +
        '                </div>\n' +
        '\n' +
        '                <!-- Card -->\n' +
        '                <div class="card border-0">\n' +
        '                  <div class="card-body">\n' +
        '\n' +
        '                    <div class="row align-items-center gx-5">\n' +
        '                      <div class="col-auto">\n' +
        '                        <a href="#" class="avatar avatar-online">\n' +
        '\n' +
        '                          <img class="avatar-img" src="/img/avatars/8.jpg" alt="">\n' +
        '\n' +
        '\n' +
        '                        </a>\n' +
        '                      </div>\n' +
        '\n' +
        '                      <div class="col">\n' +
        '                        <h5><a href="#">Elise Dennis</a></h5>\n' +
        '                        <p>online</p>\n' +
        '                      </div>\n' +
        '\n' +
        '                      <div class="col-auto">\n' +
        '                        <!-- Dropdown -->\n' +
        '                        <div class="dropdown">\n' +
        '                          <a class="icon text-muted" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">\n' +
        '                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg>\n' +
        '                          </a>\n' +
        '\n' +
        '                          <ul class="dropdown-menu">\n' +
        '                            <li><a class="dropdown-item" href="#">New message</a></li>\n' +
        '                            <li><a class="dropdown-item" href="#">Edit contact</a>\n' +
        '                            </li>\n' +
        '                            <li>\n' +
        '                              <hr class="dropdown-divider">\n' +
        '                            </li>\n' +
        '                            <li>\n' +
        '                              <a class="dropdown-item text-danger" href="#">Block user</a>\n' +
        '                            </li>\n' +
        '                          </ul>\n' +
        '                        </div>\n' +
        '                      </div>\n' +
        '\n' +
        '                    </div>\n' +
        '\n' +
        '                  </div>\n' +
        '                </div>\n' +
        '                <!-- Card -->\n' +
        '\n' +
        '                <div class="my-5">\n' +
        '                  <small class="text-uppercase text-muted">M</small>\n' +
        '                </div>\n' +
        '\n' +
        '                <!-- Card -->\n' +
        '                <div class="card border-0">\n' +
        '                  <div class="card-body">\n' +
        '\n' +
        '                    <div class="row align-items-center gx-5">\n' +
        '                      <div class="col-auto">\n' +
        '                        <a href="#" class="avatar ">\n' +
        '\n' +
        '\n' +
        '                          <span class="avatar-text">M</span>\n' +
        '\n' +
        '                        </a>\n' +
        '                      </div>\n' +
        '\n' +
        '                      <div class="col">\n' +
        '                        <h5><a href="#">Marshall Wallaker</a></h5>\n' +
        '                        <p>last seen within a month</p>\n' +
        '                      </div>\n' +
        '\n' +
        '                      <div class="col-auto">\n' +
        '                        <!-- Dropdown -->\n' +
        '                        <div class="dropdown">\n' +
        '                          <a class="icon text-muted" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">\n' +
        '                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg>\n' +
        '                          </a>\n' +
        '\n' +
        '                          <ul class="dropdown-menu">\n' +
        '                            <li><a class="dropdown-item" href="#">New message</a></li>\n' +
        '                            <li><a class="dropdown-item" href="#">Edit contact</a>\n' +
        '                            </li>\n' +
        '                            <li>\n' +
        '                              <hr class="dropdown-divider">\n' +
        '                            </li>\n' +
        '                            <li>\n' +
        '                              <a class="dropdown-item text-danger" href="#">Block user</a>\n' +
        '                            </li>\n' +
        '                          </ul>\n' +
        '                        </div>\n' +
        '                      </div>\n' +
        '\n' +
        '                    </div>\n' +
        '\n' +
        '                  </div>\n' +
        '                </div>\n' +
        '                <!-- Card --><!-- Card -->\n' +
        '                <div class="card border-0">\n' +
        '                  <div class="card-body">\n' +
        '\n' +
        '                    <div class="row align-items-center gx-5">\n' +
        '                      <div class="col-auto">\n' +
        '                        <a href="#" class="avatar ">\n' +
        '\n' +
        '                          <img class="avatar-img" src="/img/avatars/11.jpg" alt="">\n' +
        '\n' +
        '\n' +
        '                        </a>\n' +
        '                      </div>\n' +
        '\n' +
        '                      <div class="col">\n' +
        '                        <h5><a href="#">Mila White</a></h5>\n' +
        '                        <p>last seen a long time ago</p>\n' +
        '                      </div>\n' +
        '\n' +
        '                      <div class="col-auto">\n' +
        '                        <!-- Dropdown -->\n' +
        '                        <div class="dropdown">\n' +
        '                          <a class="icon text-muted" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">\n' +
        '                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg>\n' +
        '                          </a>\n' +
        '\n' +
        '                          <ul class="dropdown-menu">\n' +
        '                            <li><a class="dropdown-item" href="#">New message</a></li>\n' +
        '                            <li><a class="dropdown-item" href="#">Edit contact</a>\n' +
        '                            </li>\n' +
        '                            <li>\n' +
        '                              <hr class="dropdown-divider">\n' +
        '                            </li>\n' +
        '                            <li>\n' +
        '                              <a class="dropdown-item text-danger" href="#">Block user</a>\n' +
        '                            </li>\n' +
        '                          </ul>\n' +
        '                        </div>\n' +
        '                      </div>\n' +
        '\n' +
        '                    </div>\n' +
        '\n' +
        '                  </div>\n' +
        '                </div>\n' +
        '                <!-- Card -->\n' +
        '\n' +
        '                <div class="my-5">\n' +
        '                  <small class="text-uppercase text-muted">O</small>\n' +
        '                </div>\n' +
        '\n' +
        '                <!-- Card -->\n' +
        '                <div class="card border-0">\n' +
        '                  <div class="card-body">\n' +
        '\n' +
        '                    <div class="row align-items-center gx-5">\n' +
        '                      <div class="col-auto">\n' +
        '                        <a href="#" class="avatar avatar-online">\n' +
        '\n' +
        '\n' +
        '                          <span class="avatar-text">O</span>\n' +
        '\n' +
        '                        </a>\n' +
        '                      </div>\n' +
        '\n' +
        '                      <div class="col">\n' +
        '                        <h5><a href="#">Ollie Chandler</a></h5>\n' +
        '                        <p>online</p>\n' +
        '                      </div>\n' +
        '\n' +
        '                      <div class="col-auto">\n' +
        '                        <!-- Dropdown -->\n' +
        '                        <div class="dropdown">\n' +
        '                          <a class="icon text-muted" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">\n' +
        '                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg>\n' +
        '                          </a>\n' +
        '\n' +
        '                          <ul class="dropdown-menu">\n' +
        '                            <li><a class="dropdown-item" href="#">New message</a></li>\n' +
        '                            <li><a class="dropdown-item" href="#">Edit contact</a>\n' +
        '                            </li>\n' +
        '                            <li>\n' +
        '                              <hr class="dropdown-divider">\n' +
        '                            </li>\n' +
        '                            <li>\n' +
        '                              <a class="dropdown-item text-danger" href="#">Block user</a>\n' +
        '                            </li>\n' +
        '                          </ul>\n' +
        '                        </div>\n' +
        '                      </div>\n' +
        '\n' +
        '                    </div>\n' +
        '\n' +
        '                  </div>\n' +
        '                </div>\n' +
        '                <!-- Card -->\n' +
        '\n' +
        '                <div class="my-5">\n' +
        '                  <small class="text-uppercase text-muted">W</small>\n' +
        '                </div>\n' +
        '\n' +
        '                <!-- Card -->\n' +
        '                <div class="card border-0">\n' +
        '                  <div class="card-body">\n' +
        '\n' +
        '                    <div class="row align-items-center gx-5">\n' +
        '                      <div class="col-auto">\n' +
        '                        <a href="#" class="avatar ">\n' +
        '\n' +
        '                          <img class="avatar-img" src="/img/avatars/4.jpg" alt="">\n' +
        '\n' +
        '\n' +
        '                        </a>\n' +
        '                      </div>\n' +
        '\n' +
        '                      <div class="col">\n' +
        '                        <h5><a href="#">Warren White</a></h5>\n' +
        '                        <p>last seen recently</p>\n' +
        '                      </div>\n' +
        '\n' +
        '                      <div class="col-auto">\n' +
        '                        <!-- Dropdown -->\n' +
        '                        <div class="dropdown">\n' +
        '                          <a class="icon text-muted" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">\n' +
        '                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg>\n' +
        '                          </a>\n' +
        '\n' +
        '                          <ul class="dropdown-menu">\n' +
        '                            <li><a class="dropdown-item" href="#">New message</a></li>\n' +
        '                            <li><a class="dropdown-item" href="#">Edit contact</a>\n' +
        '                            </li>\n' +
        '                            <li>\n' +
        '                              <hr class="dropdown-divider">\n' +
        '                            </li>\n' +
        '                            <li>\n' +
        '                              <a class="dropdown-item text-danger" href="#">Block user</a>\n' +
        '                            </li>\n' +
        '                          </ul>\n' +
        '                        </div>\n' +
        '                      </div>\n' +
        '\n' +
        '                    </div>\n' +
        '\n' +
        '                  </div>\n' +
        '                </div>\n' +
        '                <!-- Card --><!-- Card -->\n' +
        '                <div class="card border-0">\n' +
        '                  <div class="card-body">\n' +
        '\n' +
        '                    <div class="row align-items-center gx-5">\n' +
        '                      <div class="col-auto">\n' +
        '                        <a href="#" class="avatar avatar-online">\n' +
        '\n' +
        '                          <img class="avatar-img" src="/img/avatars/7.jpg" alt="">\n' +
        '\n' +
        '\n' +
        '                        </a>\n' +
        '                      </div>\n' +
        '\n' +
        '                      <div class="col">\n' +
        '                        <h5><a href="#">William Wright</a></h5>\n' +
        '                        <p>online</p>\n' +
        '                      </div>\n' +
        '\n' +
        '                      <div class="col-auto">\n' +
        '                        <!-- Dropdown -->\n' +
        '                        <div class="dropdown">\n' +
        '                          <a class="icon text-muted" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">\n' +
        '                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg>\n' +
        '                          </a>\n' +
        '\n' +
        '                          <ul class="dropdown-menu">\n' +
        '                            <li><a class="dropdown-item" href="#">New message</a></li>\n' +
        '                            <li><a class="dropdown-item" href="#">Edit contact</a>\n' +
        '                            </li>\n' +
        '                            <li>\n' +
        '                              <hr class="dropdown-divider">\n' +
        '                            </li>\n' +
        '                            <li>\n' +
        '                              <a class="dropdown-item text-danger" href="#">Block user</a>\n' +
        '                            </li>\n' +
        '                          </ul>\n' +
        '                        </div>\n' +
        '                      </div>\n' +
        '\n' +
        '                    </div>\n' +
        '\n' +
        '                  </div>\n' +
        '                </div>\n' +
        '                <!-- Card -->\n' +
        '              </div>\n' +
        '\n' +
        '            </div>\n' +
        '          </div>\n' +
        '        </div>\n' +
        '      </div>'
}