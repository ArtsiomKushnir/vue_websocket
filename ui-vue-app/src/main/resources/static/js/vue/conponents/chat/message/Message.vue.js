export default {
    name: 'Message',
    props: ['message'],
    template:
        '<div class="message">' +
        '   <a href="#" data-bs-toggle="modal" data-bs-target="#modal-user-profile" class="avatar avatar-responsive">' +
        '       <span class="avatar-text bg-info">{{ message.receiver[0].toUpperCase() }}</span>' +
        '   </a>' +
        '   <div class="message-inner">' +
        '       <div class="message-body">' +
        '           <div class="message-content">' +
        '               <div class="message-text">' +
        '                   <p>{{ message.content }}</p>' +
        '               </div>' +
        '           </div>' +
        '       </div>' +
        '       <div class="message-footer">' +
        '           <span class="extra-small text-muted">{{ message.sentDate }}</span>' +
        '       </div>' +
        '   </div>' +
        '</div>'
}