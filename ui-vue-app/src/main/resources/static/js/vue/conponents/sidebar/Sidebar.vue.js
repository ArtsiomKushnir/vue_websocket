import CreateTab from './tabs/create/CreateTab.vue.js'
import ContactsTab from './tabs/contacts/ContactsTab.vue.js'
import ChatsTab from './tabs/chats/ChatsTab.vue.js'
import SettingsTab from './tabs/SettingsTab.vue.js'

export default {
    name: 'Sidebar',
    props: {
        wrapped: Object
    },
    methods: {
        openChat(user) {
            this.$emit('select-user', user);
        }
    },
    components: {
        CreateTab,
        ContactsTab,
        ChatsTab,
        SettingsTab
    },
    template:
        '<aside class="sidebar bg-light">' +
        '   <div class="tab-content h-100" role="tablist">' +

        '       <!--<create-tab></create-tab>-->' +
        '       <!--<contacts-tab></contacts-tab>-->' +
        '       <chats-tab :wrapped="wrapped" v-on:select-user="openChat"></chats-tab>' +
        '       <settings-tab></settings-tab>' +

        '   <div/>' +
        '</aside>'
}