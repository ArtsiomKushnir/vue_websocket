export default {
    name: 'ChatsItem',
    template:
        '<li class="nav-item d-xl-block flex-xl-grow-1">' +
        '   <a class="nav-link py-0 py-lg-8 active" id="tab-chats" href="#tab-content-chats" title="Chats" data-bs-toggle="tab" role="tab" aria-selected="true">' +
        '       <div class="icon icon-xl icon-badged">' +
        '           <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" ' +
        '               fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" ' +
        '               class="feather feather-message-square">' +
        '               <path d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z"></path>' +
        '           </svg>' +
        '           <div class="badge badge-circle bg-primary">' +
        '               <span>4</span>' +
        '           </div>' +
        '       </div>' +
        '   </a>' +
        '</li>'
}