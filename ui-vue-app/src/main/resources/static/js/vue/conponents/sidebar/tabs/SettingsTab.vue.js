export default {
    name: 'SettingsTab',
    template:
        '<div class="tab-pane fade h-100" id="tab-content-settings" role="tabpanel">\n' +
        '                        <div class="d-flex flex-column h-100">\n' +
        '                            <div class="hide-scrollbar">\n' +
        '                                <div class="container py-8">\n' +
        '\n' +
        '                                    <!-- Title -->\n' +
        '                                    <div class="mb-8">\n' +
        '                                        <h2 class="fw-bold m-0">Settings</h2>\n' +
        '                                    </div>\n' +
        '\n' +
        '                                    <!-- Search -->\n' +
        '                                    <div class="mb-6">\n' +
        '                                        <form action="#">\n' +
        '                                            <div class="input-group">\n' +
        '                                                <div class="input-group-text">\n' +
        '                                                    <div class="icon icon-lg">\n' +
        '                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>\n' +
        '                                                    </div>\n' +
        '                                                </div>\n' +
        '\n' +
        '                                                <input type="text" class="form-control form-control-lg ps-0" placeholder="Search messages or users" aria-label="Search for messages or users...">\n' +
        '                                            </div>\n' +
        '                                        </form>\n' +
        '                                    </div>\n' +
        '\n' +
        '                                    <!-- Profile -->\n' +
        '                                    <div class="card border-0">\n' +
        '                                        <div class="card-body">\n' +
        '                                            <div class="row align-items-center gx-5">\n' +
        '                                                <div class="col-auto">\n' +
        '                                                    <div class="avatar">\n' +
        '                                                        <span class="avatar-text bg-success">U</span>' +

        '                                                        <div class="badge badge-circle bg-secondary border-outline position-absolute bottom-0 end-0">\n' +
        '                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-image"><rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect><circle cx="8.5" cy="8.5" r="1.5"></circle><polyline points="21 15 16 10 5 21"></polyline></svg>\n' +
        '                                                        </div>\n' +
        '                                                        <input id="upload-profile-photo" class="d-none" type="file">\n' +
        '                                                        <label class="stretched-label mb-0" for="upload-profile-photo"></label>\n' +
        '                                                    </div>\n' +
        '                                                </div>\n' +
        '                                                <div class="col">\n' +
        '                                                    <h5>William Pearson</h5>\n' +
        '                                                    <p>wright@studio.com</p>\n' +
        '                                                </div>\n' +
        '                                                <div class="col-auto">\n' +
        '                                                    <a href="#" class="text-muted">\n' +
        '                                                        <div class="icon">\n' +
        '                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-log-out"><path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path><polyline points="16 17 21 12 16 7"></polyline><line x1="21" y1="12" x2="9" y2="12"></line></svg>\n' +
        '                                                        </div>\n' +
        '                                                    </a>\n' +
        '                                                </div>\n' +
        '                                            </div>\n' +
        '                                        </div>\n' +
        '                                    </div>\n' +
        '                                    <!-- Profile -->\n' +
        '\n' +
        '                                    <!-- Account -->\n' +
        '                                    <div class="mt-8">\n' +
        '                                        <div class="d-flex align-items-center mb-4 px-6">\n' +
        '                                            <small class="text-muted me-auto">Account</small>\n' +
        '                                        </div>\n' +
        '\n' +
        '                                        <div class="card border-0">\n' +
        '                                            <div class="card-body py-2">\n' +
        '                                                <!-- Accordion -->\n' +
        '                                                <div class="accordion accordion-flush" id="accordion-profile">\n' +
        '                                                    <div class="accordion-item">\n' +
        '                                                        <div class="accordion-header" id="accordion-profile-1">\n' +
        '                                                            <a href="#" class="accordion-button text-reset collapsed" data-bs-toggle="collapse" data-bs-target="#accordion-profile-body-1" aria-expanded="false" aria-controls="accordion-profile-body-1">\n' +
        '                                                                <div>\n' +
        '                                                                    <h5>Profile settings</h5>\n' +
        '                                                                    <p>Change your profile settings</p>\n' +
        '                                                                </div>\n' +
        '                                                            </a>\n' +
        '                                                        </div>\n' +
        '\n' +
        '                                                        <div id="accordion-profile-body-1" class="accordion-collapse collapse" aria-labelledby="accordion-profile-1" data-parent="#accordion-profile">\n' +
        '                                                            <div class="accordion-body">\n' +
        '                                                                <div class="form-floating mb-6">\n' +
        '                                                                    <input type="text" class="form-control" id="profile-name" placeholder="Name">\n' +
        '                                                                    <label for="profile-name">Name</label>\n' +
        '                                                                </div>\n' +
        '\n' +
        '                                                                <div class="form-floating mb-6">\n' +
        '                                                                    <input type="email" class="form-control" id="profile-email" placeholder="Email address">\n' +
        '                                                                    <label for="profile-email">Email</label>\n' +
        '                                                                </div>\n' +
        '\n' +
        '                                                                <div class="form-floating mb-6">\n' +
        '                                                                    <input type="text" class="form-control" id="profile-phone" placeholder="Phone">\n' +
        '                                                                    <label for="profile-phone">Phone</label>\n' +
        '                                                                </div>\n' +
        '\n' +
        '                                                                <div class="form-floating mb-6">\n' +
        '                                                                    <textarea class="form-control" placeholder="Bio" id="profile-bio" data-autosize="true" style="min-height: 120px; overflow: hidden; overflow-wrap: break-word; resize: none;"></textarea>\n' +
        '                                                                    <label for="profile-bio">Bio</label>\n' +
        '                                                                </div>\n' +
        '\n' +
        '                                                                <button type="button" class="btn btn-block btn-lg btn-primary w-100">Save</button>\n' +
        '                                                            </div>\n' +
        '                                                        </div>\n' +
        '                                                    </div>\n' +
        '\n' +
        '                                                    <div class="accordion-item">\n' +
        '                                                        <div class="accordion-header" id="accordion-profile-2">\n' +
        '                                                            <a href="#" class="accordion-button text-reset collapsed" data-bs-toggle="collapse" data-bs-target="#accordion-profile-body-2" aria-expanded="false" aria-controls="accordion-profile-body-2">\n' +
        '                                                                <div>\n' +
        '                                                                    <h5>Connected accounts</h5>\n' +
        '                                                                    <p>Connect with your accounts</p>\n' +
        '                                                                </div>\n' +
        '                                                            </a>\n' +
        '                                                        </div>\n' +
        '\n' +
        '                                                        <div id="accordion-profile-body-2" class="accordion-collapse collapse" aria-labelledby="accordion-profile-2" data-parent="#accordion-profile">\n' +
        '                                                            <div class="accordion-body">\n' +
        '                                                                <div class="form-floating mb-6">\n' +
        '                                                                    <input type="text" class="form-control" id="profile-twitter" placeholder="Twitter">\n' +
        '                                                                    <label for="profile-twitter">Twitter</label>\n' +
        '                                                                </div>\n' +
        '\n' +
        '                                                                <div class="form-floating mb-6">\n' +
        '                                                                    <input type="text" class="form-control" id="profile-facebook" placeholder="Facebook">\n' +
        '                                                                    <label for="profile-facebook">Facebook</label>\n' +
        '                                                                </div>\n' +
        '\n' +
        '                                                                <div class="form-floating mb-6">\n' +
        '                                                                    <input type="text" class="form-control" id="profile-instagram" placeholder="Instagram">\n' +
        '                                                                    <label for="profile-instagram">Instagram</label>\n' +
        '                                                                </div>\n' +
        '\n' +
        '                                                                <button type="button" class="btn btn-block btn-lg btn-primary w-100">Save</button>\n' +
        '                                                            </div>\n' +
        '                                                        </div>\n' +
        '                                                    </div>\n' +
        '\n' +
        '                                                    <!-- Switch -->\n' +
        '                                                    <div class="accordion-item">\n' +
        '                                                        <div class="accordion-header">\n' +
        '                                                            <div class="row align-items-center">\n' +
        '                                                                <div class="col">\n' +
        '                                                                    <h5>Appearance</h5>\n' +
        '                                                                    <p>Choose light or dark theme</p>\n' +
        '                                                                </div>\n' +
        '                                                                <div class="col-auto">\n' +
        '                                                                    <a class="switcher-btn text-reset" href="#!" title="Themes">\n' +
        '                                                                        <div class="switcher-icon switcher-icon-dark icon icon-lg d-none" data-theme-mode="dark">\n' +
        '                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-moon"><path d="M21 12.79A9 9 0 1 1 11.21 3 7 7 0 0 0 21 12.79z"></path></svg>\n' +
        '                                                                        </div>\n' +
        '                                                                        <div class="switcher-icon switcher-icon-light icon icon-lg" data-theme-mode="light">\n' +
        '                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-sun"><circle cx="12" cy="12" r="5"></circle><line x1="12" y1="1" x2="12" y2="3"></line><line x1="12" y1="21" x2="12" y2="23"></line><line x1="4.22" y1="4.22" x2="5.64" y2="5.64"></line><line x1="18.36" y1="18.36" x2="19.78" y2="19.78"></line><line x1="1" y1="12" x2="3" y2="12"></line><line x1="21" y1="12" x2="23" y2="12"></line><line x1="4.22" y1="19.78" x2="5.64" y2="18.36"></line><line x1="18.36" y1="5.64" x2="19.78" y2="4.22"></line></svg>\n' +
        '                                                                        </div>\n' +
        '                                                                    </a>\n' +
        '                                                                </div>\n' +
        '                                                            </div>\n' +
        '                                                        </div>\n' +
        '                                                    </div>\n' +
        '                                                </div>\n' +
        '                                            </div>\n' +
        '                                        </div>\n' +
        '                                    </div>\n' +
        '                                    <!-- Account -->\n' +
        '\n' +
        '                                    <!-- Security -->\n' +
        '                                    <div class="mt-8">\n' +
        '                                        <div class="d-flex align-items-center my-4 px-6">\n' +
        '                                            <small class="text-muted me-auto">Security</small>\n' +
        '                                        </div>\n' +
        '\n' +
        '                                        <div class="card border-0">\n' +
        '                                            <div class="card-body py-2">\n' +
        '                                                <!-- Accordion -->\n' +
        '                                                <div class="accordion accordion-flush" id="accordion-security">\n' +
        '                                                    <div class="accordion-item">\n' +
        '                                                        <div class="accordion-header" id="accordion-security-1">\n' +
        '                                                            <a href="#" class="accordion-button text-reset collapsed" data-bs-toggle="collapse" data-bs-target="#accordion-security-body-1" aria-expanded="false" aria-controls="accordion-security-body-1">\n' +
        '                                                                <div>\n' +
        '                                                                    <h5>Password</h5>\n' +
        '                                                                    <p>Change your password</p>\n' +
        '                                                                </div>\n' +
        '                                                            </a>\n' +
        '                                                        </div>\n' +
        '\n' +
        '                                                        <div id="accordion-security-body-1" class="accordion-collapse collapse" aria-labelledby="accordion-security-1" data-parent="#accordion-security">\n' +
        '                                                            <div class="accordion-body">\n' +
        '                                                                <form action="#" autocomplete="on">\n' +
        '                                                                    <div class="form-floating mb-6">\n' +
        '                                                                        <input type="password" class="form-control" id="profile-current-password" placeholder="Current Password" autocomplete="">\n' +
        '                                                                        <label for="profile-current-password">Current Password</label>\n' +
        '                                                                    </div>\n' +
        '\n' +
        '                                                                    <div class="form-floating mb-6">\n' +
        '                                                                        <input type="password" class="form-control" id="profile-new-password" placeholder="New password" autocomplete="">\n' +
        '                                                                        <label for="profile-new-password">New password</label>\n' +
        '                                                                    </div>\n' +
        '\n' +
        '                                                                    <div class="form-floating mb-6">\n' +
        '                                                                        <input type="password" class="form-control" id="profile-verify-password" placeholder="Verify Password" autocomplete="">\n' +
        '                                                                        <label for="profile-verify-password">Verify Password</label>\n' +
        '                                                                    </div>\n' +
        '                                                                </form>\n' +
        '                                                                <button type="button" class="btn btn-block btn-lg btn-primary w-100">Save</button>\n' +
        '                                                            </div>\n' +
        '                                                        </div>\n' +
        '                                                    </div>\n' +
        '\n' +
        '                                                    <!-- Switch -->\n' +
        '                                                    <div class="accordion-item">\n' +
        '                                                        <div class="accordion-header">\n' +
        '                                                            <div class="row align-items-center">\n' +
        '                                                                <div class="col">\n' +
        '                                                                    <h5>Two-step verifications</h5>\n' +
        '                                                                    <p>Enable two-step verifications</p>\n' +
        '                                                                </div>\n' +
        '                                                                <div class="col-auto">\n' +
        '                                                                    <div class="form-check form-switch">\n' +
        '                                                                        <input class="form-check-input" type="checkbox" id="accordion-security-check-1">\n' +
        '                                                                        <label class="form-check-label" for="accordion-security-check-1"></label>\n' +
        '                                                                    </div>\n' +
        '                                                                </div>\n' +
        '                                                            </div>\n' +
        '                                                        </div>\n' +
        '                                                    </div>\n' +
        '                                                </div>\n' +
        '                                            </div>\n' +
        '                                        </div>\n' +
        '                                    </div>\n' +
        '                                    <!-- Security -->\n' +
        '\n' +
        '                                    <!-- Storage -->\n' +
        '                                    <div class="mt-8">\n' +
        '                                        <div class="d-flex align-items-center my-4 px-6">\n' +
        '                                            <small class="text-muted me-auto">Storage</small>\n' +
        '\n' +
        '                                            <div class="flex align-items-center text-muted">\n' +
        '                                                <a href="#" class="text-muted small">Clear storage</a>\n' +
        '\n' +
        '                                                <div class="icon icon-xs">\n' +
        '                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bar-chart-2"><line x1="18" y1="20" x2="18" y2="10"></line><line x1="12" y1="20" x2="12" y2="4"></line><line x1="6" y1="20" x2="6" y2="14"></line></svg>\n' +
        '                                                </div>\n' +
        '                                            </div>\n' +
        '                                        </div>\n' +
        '\n' +
        '                                        <div class="card border-0">\n' +
        '                                            <div class="card-body py-2">\n' +
        '                                                <!-- Accordion -->\n' +
        '                                                <div class="accordion accordion-flush" id="accordion-storage">\n' +
        '                                                    <div class="accordion-item">\n' +
        '                                                        <div class="accordion-header" id="accordion-storage-1">\n' +
        '                                                            <a href="#" class="accordion-button text-reset collapsed" data-bs-toggle="collapse" data-bs-target="#accordion-storage-body-1" aria-expanded="false" aria-controls="accordion-storage-body-1">\n' +
        '                                                                <div>\n' +
        '                                                                    <h5>Cache</h5>\n' +
        '                                                                    <p>Maximum cache size</p>\n' +
        '                                                                </div>\n' +
        '                                                            </a>\n' +
        '                                                        </div>\n' +
        '\n' +
        '                                                        <div id="accordion-storage-body-1" class="accordion-collapse collapse" aria-labelledby="accordion-storage-1" data-parent="#accordion-storage">\n' +
        '                                                            <div class="accordion-body">\n' +
        '                                                                <div class="row justify-content-between mb-4">\n' +
        '                                                                    <div class="col-auto">\n' +
        '                                                                        <small>2 GB</small>\n' +
        '                                                                    </div>\n' +
        '                                                                    <div class="col-auto">\n' +
        '                                                                        <small>4 GB</small>\n' +
        '                                                                    </div>\n' +
        '                                                                    <div class="col-auto">\n' +
        '                                                                        <small>6 GB</small>\n' +
        '                                                                    </div>\n' +
        '                                                                    <div class="col-auto">\n' +
        '                                                                        <small>8 GB</small>\n' +
        '                                                                    </div>\n' +
        '                                                                </div>\n' +
        '                                                                <input type="range" class="form-range" min="1" max="4" step="1" id="custom-range-1">\n' +
        '                                                            </div>\n' +
        '                                                        </div>\n' +
        '                                                    </div>\n' +
        '\n' +
        '                                                    <div class="accordion-item">\n' +
        '                                                        <div class="accordion-header">\n' +
        '                                                            <div class="row align-items-center">\n' +
        '                                                                <div class="col">\n' +
        '                                                                    <h5>Keep media</h5>\n' +
        '                                                                    <p>Photos, videos and other files</p>\n' +
        '                                                                </div>\n' +
        '                                                                <div class="col-auto">\n' +
        '                                                                    <div class="form-check form-switch">\n' +
        '                                                                        <input class="form-check-input" type="checkbox" id="accordion-storage-check-1">\n' +
        '                                                                        <label class="form-check-label" for="accordion-storage-check-1"></label>\n' +
        '                                                                    </div>\n' +
        '                                                                </div>\n' +
        '                                                            </div>\n' +
        '                                                        </div>\n' +
        '                                                    </div>\n' +
        '                                                </div>\n' +
        '                                            </div>\n' +
        '                                        </div>\n' +
        '                                    </div>\n' +
        '                                    <!-- Storage -->\n' +
        '\n' +
        '                                    <!-- Notifications -->\n' +
        '                                    <div class="mt-8">\n' +
        '                                        <div class="d-flex align-items-center my-4 px-6">\n' +
        '                                            <small class="text-muted me-auto">Notifications</small>\n' +
        '                                        </div>\n' +
        '\n' +
        '                                        <!-- Accordion -->\n' +
        '                                        <div class="card border-0">\n' +
        '                                            <div class="card-body py-2">\n' +
        '                                                <div class="accordion accordion-flush" id="accordion-notifications">\n' +
        '                                                    <div class="accordion-item">\n' +
        '                                                        <div class="accordion-header" id="accordion-notifications-1">\n' +
        '                                                            <a href="#" class="accordion-button text-reset collapsed" data-bs-toggle="collapse" data-bs-target="#accordion-notifications-body-1" aria-expanded="false" aria-controls="accordion-notifications-body-1">\n' +
        '                                                                <div>\n' +
        '                                                                    <h5>Message</h5>\n' +
        '                                                                    <p>Set custom notifications for users</p>\n' +
        '                                                                </div>\n' +
        '                                                            </a>\n' +
        '                                                        </div>\n' +
        '\n' +
        '                                                        <div id="accordion-notifications-body-1" class="accordion-collapse collapse" aria-labelledby="accordion-notifications-1" data-parent="#accordion-notifications">\n' +
        '                                                            <div class="accordion-body">\n' +
        '                                                                <div class="row align-items-center">\n' +
        '                                                                    <div class="col">\n' +
        '                                                                        <h5>Text</h5>\n' +
        '                                                                        <p>Show text in notifications</p>\n' +
        '                                                                    </div>\n' +
        '\n' +
        '                                                                    <div class="col-auto">\n' +
        '                                                                        <div class="form-check form-switch">\n' +
        '                                                                            <input class="form-check-input" type="checkbox" id="accordion-notifications-check-1">\n' +
        '                                                                            <label class="form-check-label" for="accordion-notifications-check-1"></label>\n' +
        '                                                                        </div>\n' +
        '                                                                    </div>\n' +
        '                                                                </div>\n' +
        '                                                            </div>\n' +
        '                                                        </div>\n' +
        '                                                    </div>\n' +
        '\n' +
        '                                                    <div class="accordion-item">\n' +
        '                                                        <div class="accordion-header">\n' +
        '                                                            <div class="row align-items-center">\n' +
        '                                                                <div class="col">\n' +
        '                                                                    <h5>Sound</h5>\n' +
        '                                                                    <p>Enable sound notifications</p>\n' +
        '                                                                </div>\n' +
        '                                                                <div class="col-auto">\n' +
        '                                                                    <div class="form-check form-switch">\n' +
        '                                                                        <input class="form-check-input" type="checkbox" id="accordion-notifications-check-3">\n' +
        '                                                                        <label class="form-check-label" for="accordion-notifications-check-3"></label>\n' +
        '                                                                    </div>\n' +
        '                                                                </div>\n' +
        '                                                            </div>\n' +
        '                                                        </div>\n' +
        '                                                    </div>\n' +
        '\n' +
        '                                                    <div class="accordion-item">\n' +
        '                                                        <div class="accordion-header">\n' +
        '                                                            <div class="row align-items-center">\n' +
        '                                                                <div class="col">\n' +
        '                                                                    <h5>Browser notifications</h5>\n' +
        '                                                                    <p>Enable browser notifications</p>\n' +
        '                                                                </div>\n' +
        '                                                                <div class="col-auto">\n' +
        '                                                                    <div class="form-check form-switch">\n' +
        '                                                                        <input class="form-check-input" type="checkbox" id="accordion-notifications-check-2" checked="">\n' +
        '                                                                        <label class="form-check-label" for="accordion-notifications-check-2"></label>\n' +
        '                                                                    </div>\n' +
        '                                                                </div>\n' +
        '                                                            </div>\n' +
        '                                                        </div>\n' +
        '                                                    </div>\n' +
        '                                                </div>\n' +
        '                                            </div>\n' +
        '                                        </div>\n' +
        '                                    </div>\n' +
        '                                    <!-- Notifications -->\n' +
        '\n' +
        '                                    <!-- Devices -->\n' +
        '                                    <div class="mt-8">\n' +
        '                                        <div class="d-flex align-items-center my-4 px-6">\n' +
        '                                            <small class="text-muted me-auto">Devices</small>\n' +
        '\n' +
        '                                            <div class="flex align-items-center text-muted">\n' +
        '                                                <a href="#" class="text-muted small">End all sessions</a>\n' +
        '\n' +
        '                                                <div class="icon icon-xs">\n' +
        '                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-log-out"><path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path><polyline points="16 17 21 12 16 7"></polyline><line x1="21" y1="12" x2="9" y2="12"></line></svg>\n' +
        '                                                </div>\n' +
        '                                            </div>\n' +
        '                                        </div>\n' +
        '\n' +
        '                                        <div class="card border-0">\n' +
        '                                            <div class="card-body py-3">\n' +
        '\n' +
        '                                                <ul class="list-group list-group-flush">\n' +
        '                                                    <li class="list-group-item">\n' +
        '                                                        <div class="row align-items-center">\n' +
        '                                                            <div class="col">\n' +
        '                                                                <h5>Windows ⋅ USA, Houston</h5>\n' +
        '                                                                <p>Today at 2:48 pm ⋅ Browser: Chrome</p>\n' +
        '                                                            </div>\n' +
        '                                                            <div class="col-auto">\n' +
        '                                                                <span class="text-primary extra-small">active</span>\n' +
        '                                                            </div>\n' +
        '                                                        </div>\n' +
        '                                                    </li>\n' +
        '\n' +
        '                                                    <li class="list-group-item">\n' +
        '                                                        <div class="row align-items-center">\n' +
        '                                                            <div class="col">\n' +
        '                                                                <h5>iPhone ⋅ USA, Houston</h5>\n' +
        '                                                                <p>Yesterday at 2:48 pm ⋅ Browser: Chrome</p>\n' +
        '                                                            </div>\n' +
        '                                                        </div>\n' +
        '                                                    </li>\n' +
        '                                                </ul>\n' +
        '\n' +
        '                                            </div>\n' +
        '                                        </div>\n' +
        '\n' +
        '                                    </div>\n' +
        '                                    <!-- Devices -->\n' +
        '\n' +
        '                                </div>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                    </div>'
}