import Search from "./Search.vue.js";
import CardList from "./cards/CardList.vue.js";

export default {
    name: 'ChatsTab',
    props: {
        wrapped: Object
    },
    methods: {
        openChat(user) {
            this.$emit('select-user', user);
        }
    },
    components: {
        Search,
        CardList
    },
    template:
        '<div class="tab-pane fade h-100 active show" id="tab-content-chats" role="tabpanel">' +
        '   <div class="d-flex flex-column h-100 position-relative">' +
        '       <div class="hide-scrollbar">' +
        '           <div class="container py-8">' +
        '               <div class="mb-8">' +
        '                   <h2 class="fw-bold m-0">Chats</h2>' +
        '               </div>' +

        '               <!-- Search -->' +
        '               <search></search>' +

        '               <!-- Chats -->' +
        '               <card-list :wrapped="wrapped" v-on:select-user="openChat"></card-list>' +

        '           </div>' +
        '       </div>' +
        '   </div>' +
        '</div>'
}