import UserCard from "./UserCard.vue.js";

export default {
    name: 'CardList',
    props: {
        wrapped: Object
    },
    methods: {
        openChat(user) {
            this.$emit('select-user', user);
        }
    },
    components: {
        UserCard
    },
    template:
        '<div class="card-list">' +
        '   <template v-for="user in wrapped.users">' +
        '       <user-card v-on:select-user="openChat" :user="user" :wrapped="wrapped" ></user-card>' +
        '   </template>' +
        '</div>'
}