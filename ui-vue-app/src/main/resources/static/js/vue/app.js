import Navigation from './conponents/navigation/Navigation.vue.js'
import Sidebar from './conponents/sidebar/Sidebar.vue.js'
import ChatMain from './conponents/chat/ChatMain.vue.js'

import InviteModal from './conponents/modals/InviteModal.vue.js'
import ProfileModal from './conponents/modals/ProfileModal.vue.js'
import UserProfileModal from './conponents/modals/UserProfileModal.vue.js'
import MediaPreviewModal from './conponents/modals/MediaPreviewModal.vue.js'

const { createApp } = Vue;

const isReactive = document.getElementById('reactive').attributes.reactive.value === 'true';
const userName = document.getElementById('user').attributes.user.value;

createApp({
    data() {
        return {
            wsClient: null,
            wrapped: {
                users: [],
                selectedUser: null
            },
            messages: []
        }
    },
    mounted() {
        this.getUsers();
        this.initConnect();
    },
    methods: {
        initConnect() {
            if (isReactive) {
                this.wsClient = new WebSocket('ws://localhost:8080/messages?user=' + userName);
                this.wsClient.onopen = event => {
                };
                this.wsClient.onerror = event => {
                    console.log("ERROR: " + event.data);
                };
                this.wsClient.onclose = closeEvent => {
                };

                this.wsClient.onmessage = messageEvent => {
                    let msgString = messageEvent.data;
                    console.log("message: " + msgString);
                    this.messages.push(JSON.parse(msgString));
                    this.getUserMessages();
                };

            } else {
                this.wsClient = Stomp.over(new SockJS('/ws'));
                this.wsClient.connect(
                    {},
                    frame => {
                        this.wsClient.subscribe("/user/queue/errors", message => {
                            alert("Error " + message.body);
                        }, {});

                        this.wsClient.subscribe("/user/queue/messages", message => {
                            this.messages.push(JSON.parse(message.body));
                        }, {});
                    },
                    error => {
                        alert('Error: ' + error);
                    }
                );
            }
        },
        sendMessage(message) {
            if (isReactive) {
                this.wsClient.send(JSON.stringify(message));
            } else {
                this.wsClient.send("/app/messages",
                    {},
                    JSON.stringify(message));
            }
        },
        openChat(user) {
            this.wrapped.selectedUser = user;
            this.getUserMessages();
        },
        getUserMessages() {
            axios
                .get('/messages/' + this.wrapped.selectedUser.name)
                .then(response => {
                    this.messages = (response.data);
                })
                .catch(error => console.log(error));
        },
        getUsers() {
            axios
                .get('/users')
                .then(response => {
                    this.wrapped.users = response.data;
                })
                .catch(error => console.log(error));
        }
    },
    components: {
        Navigation,
        Sidebar,
        ChatMain,
        InviteModal,
        ProfileModal,
        UserProfileModal,
        MediaPreviewModal
    }
}).mount('#app');