DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS messages;

CREATE TABLE IF NOT EXISTS users (
    name                VARCHAR(50)     NOT NULL PRIMARY KEY,
    password            VARCHAR(500)    NOT NULL
);

INSERT INTO users (name, password)
VALUES ('user', 'user'),
       ('user1', 'user'),
       ('user2', 'user'),
       ('user3', 'user');

CREATE TABLE IF NOT EXISTS messages (
    id                  BIGINT          AUTO_INCREMENT PRIMARY KEY,
    sender              VARCHAR(50)     NOT NULL,
    receiver            VARCHAR(50)     NOT NULL,
    content             VARCHAR(2000)   NOT NULL,
    sent_date           TIMESTAMP       DEFAULT CURRENT_TIMESTAMP
);