package org.godeltech.research.messenger.rest;

import lombok.RequiredArgsConstructor;
import org.godeltech.research.messenger.entity.Message;
import org.godeltech.research.messenger.entity.User;
import org.godeltech.research.messenger.service.MessagesService;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;

import static org.godeltech.research.messenger.service.UserService.SESSION_USER_KEY;

@RestController
@RequestMapping("/messages")
@SessionAttributes(SESSION_USER_KEY)
@RequiredArgsConstructor
public class MessagesRestController {
    private final MessagesService messagesService;

    @GetMapping("/{toUser}")
    public Flux<Message> getUserMessages(User user, @PathVariable String toUser) {
        return messagesService.getUserMessages(user.getName(), toUser);
    }
}