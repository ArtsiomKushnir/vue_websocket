package org.godeltech.research.messenger.repository;

import org.godeltech.research.messenger.entity.Message;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.util.Collection;

@Repository
public interface MessagesRepository extends ReactiveCrudRepository<Message, Long> {

    Flux<Message> findAllBySenderInAndReceiverInOrderBySentDate(Collection<String> sender, Collection<String> receiver);
}