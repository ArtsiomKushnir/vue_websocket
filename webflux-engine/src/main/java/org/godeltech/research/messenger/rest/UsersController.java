package org.godeltech.research.messenger.rest;

import lombok.RequiredArgsConstructor;
import org.godeltech.research.messenger.entity.User;
import org.godeltech.research.messenger.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;
import reactor.core.publisher.Flux;

import static org.godeltech.research.messenger.service.UserService.SESSION_USER_KEY;

@RestController
@RequestMapping("/users")
@SessionAttributes(SESSION_USER_KEY)
@RequiredArgsConstructor
public class UsersController {

    private final UserService userService;

    @GetMapping
    public Flux<User> getUsers(User user) {
        return userService.getEnabledUsers(user.getName());
    }
}