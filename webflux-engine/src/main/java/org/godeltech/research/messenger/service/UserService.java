package org.godeltech.research.messenger.service;

import lombok.RequiredArgsConstructor;
import org.godeltech.research.messenger.dto.UserCredentialsDto;
import org.godeltech.research.messenger.entity.User;
import org.godeltech.research.messenger.repository.UserRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class UserService {

    public static final String SESSION_USER_KEY = "user";

    private final UserRepository userRepository;

    public Flux<User> getEnabledUsers(String userName) {
        return userRepository.findAllByNameIsNot(userName);
    }

    public Mono<User> register(final UserCredentialsDto userCredentialsDto) {
        return userRepository.save(User.builder()
                .name(userCredentialsDto.getUsername())
                .password(userCredentialsDto.getPassword())
                .build());
    }

    public Mono<User> getUser(final UserCredentialsDto userCredentialsDto) {
        return userRepository.findByNameAndPassword(userCredentialsDto.getUsername(), userCredentialsDto.getPassword());
    }

}