package org.godeltech.research.messenger.controller;

import lombok.RequiredArgsConstructor;
import org.godeltech.research.messenger.dto.UserCredentialsDto;
import org.godeltech.research.messenger.entity.User;
import org.godeltech.research.messenger.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebSession;
import reactor.core.publisher.Mono;

import static org.godeltech.research.messenger.service.UserService.SESSION_USER_KEY;

@Controller
@RequiredArgsConstructor
@SessionAttributes(SESSION_USER_KEY)
public class AppController {

    private final UserService userService;

    @GetMapping(value = {"", "/", "/index"})
    public Mono<String> mainPage(User user, WebSession webSession, ServerWebExchange swe) {
        return user.getName() != null ? Mono.just("index") : Mono.just("redirect:/login");
    }

    @GetMapping(value = "/login")
    public Mono<String> loginPage() {
        return Mono.just("login");
    }

    @PostMapping(value = "/login")
    public Mono<String> loginUser(User user, WebSession webSession, ServerWebExchange swe,
                                  @ModelAttribute final UserCredentialsDto userCredentialsDto) {
        return userService.getUser(userCredentialsDto)
                .flatMap(user1 -> {
                    if (user1.getName() != null) {
                        user.setName(user1.getName());
                        return Mono.just("redirect:/");
                    }
                    return Mono.just("redirect:/login");
                });
    }

    @GetMapping(value = "/register")
    public Mono<String> registerPage() {
        return Mono.just("register");
    }

    @PostMapping(value = "/register")
    public Mono<String> registerUser(User user, @ModelAttribute UserCredentialsDto userCredentialsDto) {
        return userService.register(userCredentialsDto)
                .flatMap(user1 -> {
                    user.setName(user1.getName());
                    return Mono.just("redirect:/");
                });
    }

    @GetMapping(value = "/logout")
    public Mono<String> logoutPage(SessionStatus status) {
        status.setComplete();
        return Mono.just("redirect:/");
    }

    @ModelAttribute("reactive")
    public Boolean reactive() {
        return true;
    }

    @ModelAttribute("userName")
    public String user(User user) {
        return user.getName();
    }
}
