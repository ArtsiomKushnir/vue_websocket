package org.godeltech.research.messenger.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.godeltech.research.messenger.dto.MessageDto;
import org.godeltech.research.messenger.entity.Message;
import org.godeltech.research.messenger.service.MessagesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.WebSocketSession;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;

import java.util.function.Consumer;

@Component
@RequiredArgsConstructor
@Slf4j
public class MessageHandler implements WebSocketHandler {

    private final MessagesService messagesService;

    private final ObjectMapper objectMapper;

    @Override
    public Mono<Void> handle(WebSocketSession session) {
        String userName = UriComponentsBuilder
                .fromUri(session.getHandshakeInfo()
                        .getUri()).build()
                .getQueryParams().toSingleValueMap().get("user");

        log.info("Websocket connected for userName: " + userName);

        Flux<WebSocketMessage> inbound = session.receive()
                .map(webSocketMessage -> mapMessage(webSocketMessage.getPayloadAsText()))
                .flatMap(messageDto -> messagesService.save(messageDto, userName))
                .map(message -> session.textMessage(mapMessage(message)));
        return session.send(inbound);
    }

    @SneakyThrows
    private MessageDto mapMessage(final String msgString) {
        return objectMapper.readValue(msgString, MessageDto.class);
    }

    @SneakyThrows
    private String mapMessage(final Message msgString) {
        return objectMapper.writeValueAsString(msgString);
    }
}
