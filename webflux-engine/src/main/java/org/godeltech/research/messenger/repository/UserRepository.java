package org.godeltech.research.messenger.repository;

import org.godeltech.research.messenger.entity.User;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@Repository
public interface UserRepository extends ReactiveCrudRepository<User, String> {
    Flux<User> findAllByNameIsNot(String name);

    Mono<User> findByNameAndPassword(String username, String password);
}
