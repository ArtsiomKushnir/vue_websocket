package org.godeltech.research.messenger.rest;

import jakarta.servlet.http.HttpSession;
import lombok.RequiredArgsConstructor;
import org.godeltech.research.messenger.entity.Message;
import org.godeltech.research.messenger.service.MessagesService;
import org.godeltech.research.messenger.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/messages")
@RequiredArgsConstructor
public class MessagesRestController {
    private final UserService userService;
    private final MessagesService messagesService;

    @GetMapping("/{toUser}")
    public List<Message> getUserMessages(final HttpSession session, @PathVariable String toUser) {
        var user = userService.getSessionUser(session);
        return messagesService.getUserMessages(user.getName(), toUser);
    }
}