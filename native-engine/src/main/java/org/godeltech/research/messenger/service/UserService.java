package org.godeltech.research.messenger.service;

import jakarta.servlet.http.HttpSession;
import lombok.RequiredArgsConstructor;
import org.godeltech.research.messenger.dto.UserCredentialsDto;
import org.godeltech.research.messenger.entity.User;
import org.godeltech.research.messenger.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {

    public static final String SESSION_USER_KEY = "user";

    private final UserRepository userRepository;

    public List<User> getEnabledUsers(String userName) {
        return userRepository.findAllByNameIsNot(userName);
    }

    public User register(final UserCredentialsDto userCredentialsDto) {
        return userRepository.save(User.builder()
                .name(userCredentialsDto.getUsername())
                .password(userCredentialsDto.getPassword())
                .build());
    }

    public Optional<User> getUser(final UserCredentialsDto userCredentialsDto) {
        return userRepository.findByNameAndPassword(userCredentialsDto.getUsername(), userCredentialsDto.getPassword());
    }

    public void login(final HttpSession session, final User user) {
        session.setAttribute(SESSION_USER_KEY, user);
    }

    public void logout(final HttpSession session) {
        session.setAttribute(SESSION_USER_KEY, null);
    }

    public User getSessionUser(final HttpSession session) {
        return (User) session.getAttribute(SESSION_USER_KEY);
    }

    public boolean isAuthorized(final HttpSession session) {
        return session.getAttribute(SESSION_USER_KEY) != null;
    }
}