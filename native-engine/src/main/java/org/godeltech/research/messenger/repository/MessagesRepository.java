package org.godeltech.research.messenger.repository;

import org.godeltech.research.messenger.entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessagesRepository extends JpaRepository<Message, Long> {

    @Query(value = "SELECT * " +
            "FROM messages " +
            "WHERE sender IN (:sender, :receiver) " +
            "AND receiver IN(:sender, :receiver) " +
            "ORDER BY sent_date;", nativeQuery = true)
    List<Message> getUsersConversation(@Param("sender") String sender, @Param("receiver") String receiver);
}