package org.godeltech.research.messenger.config;

import jakarta.servlet.http.HttpSession;
import org.godeltech.research.messenger.entity.User;
import org.godeltech.research.messenger.model.StompPrincipal;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;

import java.security.Principal;
import java.util.Map;

import static org.godeltech.research.messenger.service.UserService.SESSION_USER_KEY;

public class PrincipalHandshakeHandler extends DefaultHandshakeHandler {
    @Override
    protected Principal determineUser(ServerHttpRequest request,
                                      WebSocketHandler wsHandler,
                                      Map<String, Object> attributes) {
        HttpSession session = ((ServletServerHttpRequest) request).getServletRequest().getSession();
        var user = (User) session.getAttribute(SESSION_USER_KEY);
        return new StompPrincipal(user.getName());
    }
}
