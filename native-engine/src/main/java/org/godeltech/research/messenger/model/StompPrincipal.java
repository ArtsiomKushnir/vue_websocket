package org.godeltech.research.messenger.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.security.Principal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StompPrincipal implements Principal {

    private String name;

    @Override
    public String getName() {
        return this.name;
    }
}
