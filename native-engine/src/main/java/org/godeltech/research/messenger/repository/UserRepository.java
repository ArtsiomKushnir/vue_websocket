package org.godeltech.research.messenger.repository;


import org.godeltech.research.messenger.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, String> {
    List<User> findAllByNameIsNot(String name);
    Optional<User> findByNameAndPassword(String username, String password);
}
