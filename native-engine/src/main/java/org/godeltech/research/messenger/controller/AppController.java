package org.godeltech.research.messenger.controller;

import jakarta.servlet.http.HttpSession;
import lombok.RequiredArgsConstructor;
import org.godeltech.research.messenger.dto.UserCredentialsDto;
import org.godeltech.research.messenger.entity.User;
import org.godeltech.research.messenger.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Optional;

import static org.godeltech.research.messenger.service.UserService.SESSION_USER_KEY;

@Controller
@RequiredArgsConstructor
public class AppController {

    private final UserService userService;

    @GetMapping(value = {"", "/", "/index"})
    public String mainPage(final HttpSession session) {
        return userService.isAuthorized(session) ? "index" : "redirect:/login";
    }

    @GetMapping(value = "/login")
    public String loginPage() {
        return "login";
    }

    @PostMapping(value = "/login")
    public String loginUser(final HttpSession session, @ModelAttribute final UserCredentialsDto userCredentialsDto) {
        Optional<User> optionalUser = userService.getUser(userCredentialsDto);
        if (optionalUser.isPresent()) {
            userService.login(session, optionalUser.get());
            return "redirect:/";
        } else {
            return "redirect:/login";
        }
    }

    @GetMapping(value = "/register")
    public String registerPage() {
        return "register";
    }

    @PostMapping(value = "/register")
    public String registerUser(final HttpSession session, @ModelAttribute UserCredentialsDto userCredentialsDto) {
        var user = userService.register(userCredentialsDto);
        userService.login(session, user);
        return "redirect:/";
    }

    @GetMapping(value = "/logout")
    public String logoutPage(final HttpSession session) {
        userService.logout(session);
        return "redirect:/";
    }

    @ModelAttribute("reactive")
    public Boolean reactive() {
        return false;
    }

    @ModelAttribute("userName")
    public String user(final HttpSession session) {

        var user = (User) session.getAttribute(SESSION_USER_KEY);

        return user != null ? user.getName() : "";
    }
}
