package org.godeltech.research.messenger.rest;

import jakarta.servlet.http.HttpSession;
import lombok.RequiredArgsConstructor;
import org.godeltech.research.messenger.entity.User;
import org.godeltech.research.messenger.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UsersController {

    private final UserService userService;

    @GetMapping
    public List<User> getUsers(final HttpSession session) {
        var user = userService.getSessionUser(session);
        return userService.getEnabledUsers(user.getName());
    }
}