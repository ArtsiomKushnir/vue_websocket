package org.godeltech.research.messenger.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.godeltech.research.messenger.dto.MessageDto;
import org.godeltech.research.messenger.service.MessagesService;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;

import java.security.Principal;

@Slf4j
@Controller
@RequiredArgsConstructor
public class MessageController {
    private final MessagesService messagesService;
    private final SimpMessageSendingOperations messagingTemplate;

    @MessageMapping("/messages")
    public void messages(final MessageDto messageDto, final Principal principal) {
        var message = messagesService.save(messageDto, principal.getName());

        messagingTemplate.convertAndSendToUser(messageDto.getTo(), "/queue/messages",
                message);

        messagingTemplate.convertAndSendToUser(principal.getName(), "/queue/messages",
                message);
    }

    @MessageExceptionHandler
    @SendToUser("/queue/errors")
    public String handleException(Throwable exception) {
        log.error(exception.getMessage());
        return exception.getMessage();
    }
}