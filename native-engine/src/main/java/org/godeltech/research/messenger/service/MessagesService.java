package org.godeltech.research.messenger.service;

import lombok.RequiredArgsConstructor;
import org.godeltech.research.messenger.dto.MessageDto;
import org.godeltech.research.messenger.entity.Message;
import org.godeltech.research.messenger.repository.MessagesRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.util.HtmlUtils;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MessagesService {

    private final MessagesRepository messagesRepository;

    public List<Message> getUserMessages(String fromUser, String toUser) {
        return messagesRepository.getUsersConversation(fromUser, toUser);
    }

    public Message save(final MessageDto messageDto, final String senderName) {
        return messagesRepository.save(Message.builder()
                .sender(senderName)
                .receiver(messageDto.getTo())
                .content(HtmlUtils.htmlEscape(messageDto.getContent()))
                .sentDate(LocalDateTime.now())
                .build());
    }
}