## rnd_poc_spring_react_websocket_vue_messanger

### RUN:
##### Windows:
    .\mvnw.cmd clean -pl native-engine -am spring-boot:run
    .\mvnw.cmd clean -pl webflux-engine -am spring-boot:run

### LINKS:
    UI Template:
    https://offsetcode.com/themes/messenger/2.2.0/index.html

## How to create service on Linux:

sudo nano /etc/systemd/system/messenger.service

``` [Unit]
    Description=messenger
    
    [Service]
    Type=simple
    ExecStart=/bin/bash /home/pi/PROJECTS/vue_websocket/messenger.sh
    
    [Install]
    WantedBy=multi-user.target
```

<pre>
sudo systemctl start messenger.service
sudo systemctl status messenger.service
</pre>